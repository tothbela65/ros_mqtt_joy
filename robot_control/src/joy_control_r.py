#!/usr/bin/env python

import rospy
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from std_msgs.msg import Int32
from niryo_robot_commander.msg import RobotCommand
from sensor_msgs.msg import JointState
import actionlib
from niryo_robot_msgs.msg import CommandStatus
from actionlib_msgs.msg import GoalStatus
from niryo_robot_commander.msg import RobotMoveAction, RobotMoveGoal

import config

# MQTT
import paho.mqtt.client as mqtt
# from paho.mqtt import publish as MQTT_pub
from paho.mqtt import publish

#MQTT connect
MQTTclient = mqtt.Client(config.Client_Name, clean_session=True)

# ROS class
class ROSInter:
    def __init__(self):
        # Start a ROS node
        rospy.init_node('ControlNode')

        self.__tool_command_list = rospy.get_param("/niryo_robot_tools/command_list")
        self.__current_tool_id = 0
        self.__joints = None
        self.__robot_action_server_name = '/niryo_robot_commander/robot_action'
        self.__robot_action_server_client = actionlib.SimpleActionClient(self.__robot_action_server_name, RobotMoveAction)
        self.__action_connection_timeout = rospy.get_param("/niryo_robot/python_ros_wrapper/action_connection_timeout")
        self.__action_execute_timeout = rospy.get_param("/niryo_robot/python_ros_wrapper/action_execute_timeout")


        # Subsribe for topic
        self.JointStates_sub = rospy.Subscriber(config.JointStatesTopic, JointState, self.__callback_sub_joint_states)
        self.ToolID_sub = rospy.Subscriber(config.ToolIDTopic, Int32, self.callback_sub_current_tool_id)


        # Create publisher
        self.movement_topic_pub = rospy.Publisher(config.MovementTopic, JointTrajectory, queue_size=1)


    # Publish Move Joints
    def move_joints(self, joints, time):
        msg = JointTrajectory()
        msg.joint_names = ['joint_1', 'joint_2', 'joint_3', 'joint_4', 'joint_5', 'joint_6']
        point = JointTrajectoryPoint()
        point.positions = joints
        point.time_from_start = rospy.Duration(time)
        msg.points = [ point ]
        self.movement_topic_pub.publish(msg) 

    # def callback functions
    def __callback_sub_joint_states(self, msg):
        self.__joints = list(msg.position[:6])
        publish_joint_status(MQTTclient, list(msg.position[:6]))

    def callback_sub_current_tool_id(self, msg):
        self.__current_tool_id = msg.data


    def get_current_tool_id(self):
        timeout = rospy.get_time() + 2.0
        while self.__current_tool_id is None:
            rospy.sleep(0.05)
            if rospy.get_time() > timeout:
                raise Exception(
                    'Timeout: could not get current tool id ')
        return self.__current_tool_id

    def open_gripper(self, speed=500):
        """
        Open gripper with a speed 'speed'

        :param speed: Default -> 500
        :type speed: int
        :return: status, message
        :rtype: (int, str)
        """
        return self.__deal_with_gripper(speed, "open_gripper")

    def close_gripper(self, speed=500):
        """
        Close gripper with a speed 'speed'

        :param speed: Default -> 500
        :type speed: int
        :return: status, message
        :rtype: (int, str)
        """
        return self.__deal_with_gripper(speed, "close_gripper")

    def __deal_with_gripper(self, speed, command_str):
        goal = RobotMoveGoal()
        goal.cmd.cmd_type = RobotCommand.TOOL_ONLY
        goal.cmd.tool_cmd.tool_id = self.get_current_tool_id()
        goal.cmd.tool_cmd.cmd_type = self.__tool_command_list[command_str]
        if "open" in command_str:
            goal.cmd.tool_cmd.gripper_open_speed = speed
        else:
            goal.cmd.tool_cmd.gripper_close_speed = speed
        return self.__execute_robot_move_action(goal)


    def __execute_robot_move_action(self, goal):
        # Connect to server
        if not self.__robot_action_server_client.wait_for_server(rospy.Duration(self.__action_connection_timeout)):
            rospy.logwarn("ROS - Failed to connect to Robot action server")

            raise Exception('Action Server is not up : {}'.format(self.__robot_action_server_name))

        # Send goal and check response
        goal_state, response = self.__send_goal_and_wait_for_completed(goal)

        if response.status == CommandStatus.GOAL_STILL_ACTIVE:
            rospy.loginfo("ROS  - Command still active: try to stop it")
            self.__robot_action_server_client.cancel_goal()
            self.__robot_action_server_client.stop_tracking_goal()
            rospy.sleep(0.2)
            rospy.loginfo("ROS  - Trying to resend command ...")
            goal_state, response = self.__send_goal_and_wait_for_completed(goal)

        if goal_state != GoalStatus.SUCCEEDED:
            self.__robot_action_server_client.stop_tracking_goal()

        if goal_state == GoalStatus.REJECTED:
            raise Exception('Goal has been rejected : {}'.format(response.message))
        elif goal_state == GoalStatus.ABORTED:
            raise Exception('Goal has been aborted : {}'.format(response.message))
        elif goal_state != GoalStatus.SUCCEEDED:
            raise Exception('Error when processing goal : {}'.format(response.message))

        return response.status, response.message

    def __send_goal_and_wait_for_completed(self, goal):
        self.__robot_action_server_client.send_goal(goal)

        if not self.__robot_action_server_client.wait_for_result(timeout=rospy.Duration(self.__action_execute_timeout)):
            self.__robot_action_server_client.cancel_goal()
            self.__robot_action_server_client.stop_tracking_goal()
            raise Exception('Action Server timeout : {}'.format(self.__robot_action_server_name))

        goal_state = self.__robot_action_server_client.get_state()
        response = self.__robot_action_server_client.get_result()

        return goal_state, response

#MQTT
def on_connect(client, userdata, flags, rc):
    print("Connected with result code" + str(rc))  # Print result of connection attempt
    client.subscribe(topic = config.MoveJoints, qos = config.QoS)
    client.subscribe(topic = config.OpenGripper, qos = config.QoS)
    client.subscribe(topic = config.CloseGripper, qos = config.QoS)

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed with QoS = " + str(granted_qos[0]))

# def on_log(client, userdata, level, buf):
#     print("Log: " ,buf)

def on_disconnect(client, userdata, rc):
   print("Disconnected from broker")


def on_message_MoveJoints(client, userdata, msg):
    print("Message received-> " + msg.topic + " " + str(msg.payload))
    msg.payload = msg.payload.decode("utf-8")
    print("MOVE")
    print(msg.payload)
    payload = msg.payload.split("/")
    duration_time = float(payload[0])
    payload_joints = payload[1:len(payload)]
    joints = list(map(float, payload_joints))
    R.move_joints(joints, duration_time)

def on_message_OpenGripper(client, userdata, msg):
    print("Message received-> " + msg.topic + " " + str(msg.payload))
    msg.payload = msg.payload.decode("utf-8")
    R.open_gripper()
    print ("Open Gripper")

def on_message_CloseGripper(client, userdata, msg):
    print("Message received-> " + msg.topic + " " + str(msg.payload))
    msg.payload = msg.payload.decode("utf-8")
    R.close_gripper()
    print ("Close Gripper")


def publish_joint_status(client, joint_states):
    tmp = []
    for joint in joint_states:
        tmp.append(joint)
    data = {
        "joints": tmp
    }
    MQTTclient.publish(
        config.Joints_Status,
        str(data),
        config.QoS,
    )

def SetUp():
    
    # MQTTclient = mqtt.Client(config.Client_Name, clean_session=True)

    MQTTclient.on_connect = on_connect  # Define callback function for successful connection
    #client.on_message = on_message # Define callback function for receipt of a message
    MQTTclient.on_subscribe = on_subscribe
    # MQTTclient.on_log = on_log
    MQTTclient.on_disconnect = on_disconnect
    MQTTclient.connect(config.BROKER_IP, config.BROKER_PORT)


    MQTTclient.message_callback_add(config.MoveJoints, on_message_MoveJoints)
    MQTTclient.message_callback_add(config.OpenGripper, on_message_OpenGripper)
    MQTTclient.message_callback_add(config.CloseGripper, on_message_CloseGripper)
    

    MQTTclient.loop_forever()


R = ROSInter()
SetUp()
