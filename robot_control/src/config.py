
#### MQTT SERVER
BROKER_IP = "127.0.0.1"
BROKER_PORT = 1883
QoS = 0


Client_Name = "Joy_Receiver"


# MQTT Topics
MoveJoints     = "/robotic_arm/MoveJoints"
OpenGripper    = "/robotic_arm/OpenGripper"
CloseGripper   = "/robotic_arm/CloseGripper"
Joints_Status  = "/robotic_arm/joints"


# ROS Topics
JointStatesTopic  = "/joint_states"
MovementTopic     = "/niryo_robot_follow_joint_trajectory_controller/command"
ToolIDTopic       = "/niryo_robot_tools/current_id"