#!/usr/bin/env python
import config
from paho.mqtt import publish
import paho.mqtt.client



def publish_move_joints(client, duration, j1, j2, j3, j4, j5, j6):
    topic = config.MoveJoints
    payload = str(duration) + "/" + str(j1) + "/" + str(j2) + "/" + str(j3) + "/" + str(j4) + "/" + str(j5) + "/" + str(j6)
    print("\t\tMovement Publisher: " + str(payload))
    publish.single (
        topic,
        payload,
        config.QoS,
        retain=False,
        hostname=config.BROKER_IP,
        port=config.BROKER_PORT
    )


def publish_open_gripper():
    topic = config.OpenGripper
    publish.single(
        topic,
        "Open Gripper",
        config.QoS,
        retain=False,
        hostname=config.BROKER_IP,
        port=config.BROKER_PORT
    )

def publish_close_gripper():
    topic = config.CloseGripper
    publish.single(
        topic,
        "Close Gripper",
        config.QoS,
        retain=False,
        hostname=config.BROKER_IP,
        port=config.BROKER_PORT
    )
