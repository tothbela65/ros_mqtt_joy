import paho.mqtt.client as mqtt


#### MQTT BROKER
BROKER_IP = "127.0.0.1"
BROKER_PORT = 1883
QoS = 0


Client_Name = "Joy_Controller"


Joints = []


# MQTT Topics
MoveJoints     = "/robotic_arm/MoveJoints"
OpenGripper    = "/robotic_arm/OpenGripper"
CloseGripper   = "/robotic_arm/CloseGripper"
Joints_Status  = "/robotic_arm/joints"
