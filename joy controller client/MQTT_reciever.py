import config
import paho.mqtt.client as mqtt
import mqttpublisher
import json

def on_connect_Joints_Status_joy(client, userdata, flags, rc):
    print("Connected with result code" + str(rc))  # Print result of connection attempt
    client.subscribe(topic = config.Joints_Status, qos = config.QoS)


def on_message_Joints_Status_joy(client, userdata, msg):
    input_joints = str(msg.payload.decode("utf-8")).replace("'",'"')
    json_joints = json.loads(input_joints)
    config.Joints = json_joints["joints"]


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed with QoS = " + str(granted_qos[0]))

# def on_log(client, userdata, level, buf):
#     print("Log: " ,buf)


client = mqtt.Client(config.Client_Name, clean_session=True)
client.on_connect = on_connect_Joints_Status_joy  # Define callback function for successful connection
#client.on_message = on_message_Joints_Status_joy  # Define callback function for receipt of a message
client.on_subscribe = on_subscribe
# client.on_log = on_log
client.connect(config.BROKER_IP, config.BROKER_PORT)

client.message_callback_add(config.Joints_Status, on_message_Joints_Status_joy)

client.loop_start()
