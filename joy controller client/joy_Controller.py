import signal
from xbox360controller import Xbox360Controller
import mqttpublisher
import config
import paho.mqtt.client as mqtt
import MQTT_reciever
import time



DEFAULT_MULTIPLIER = 0.07
deadzone = 0.000005
actual_joint_states = [0, 0, 0, 0, 0, 0]


def on_button_pressed(button):
    print('Button {0} was pressed'.format(button.name))
    if not (controller.axis_r.x or controller.axis_r.y or controller.axis_l.y or controller.axis_l.x or controller.hat.y or controller.hat.x):
        if button.name == 'button_x':
                mqttpublisher.publish_open_gripper()
                print('Open gripper')

        if button.name == 'button_b':
                mqttpublisher.publish_close_gripper()
                print('Close gripper')

        if button.name == 'button_a':
            controller.set_rumble(0.5, 0.5, 1000)
            time.sleep(1)



def on_select_pressed(button):
    global select 
    select = True


def on_button_released(button):
    print('Button {0} was released'.format(button.name))


def big_enough(a, b):
    global deadzone
    if abs(a-b) > deadzone:
        return True
    return False


def on_axis_moved(axis):
    #print('Axis {0} moved to {1} {2}'.format(axis.name, round(axis.x, 3), round(axis.y, 3)))
    print('Axis {0} moved to {1} {2}'.format(axis.name, axis.x, axis.y))
    return True


# wait for joint value 
time.sleep(0.5)



with Xbox360Controller(0, axis_threshold=0.3) as controller:
    while True:  # Cycle for joystick events

    # Current joint positions
        actual_joint_states = config.Joints
        # print("Current joint positions: ", actual_joint_states)



    # Button A events
        controller.button_a.when_pressed = on_button_pressed
        controller.button_a.when_released = on_button_released

    # Button X events
        controller.button_x.when_pressed = on_button_pressed
        controller.button_x.when_released = on_button_released

    # Button B events
        controller.button_b.when_pressed = on_button_pressed
        controller.button_b.when_released = on_button_released


        multiplier = DEFAULT_MULTIPLIER

        
        if (controller.axis_r.x or controller.axis_r.y or controller.axis_l.y or controller.axis_l.x or 
        controller.trigger_l._value or controller.trigger_r._value or 
        controller.button_trigger_l._value or controller.button_trigger_r._value):

            j1 = actual_joint_states[0]
            j2 = actual_joint_states[1]
            j3 = actual_joint_states[2]
            j4 = actual_joint_states[3]
            j5 = actual_joint_states[4]
            j6 = actual_joint_states[5]

            # Variable for publish
            x1 = actual_joint_states[0]
            x2 = actual_joint_states[1]
            x3 = actual_joint_states[2]
            x4 = actual_joint_states[3]
            x5 = actual_joint_states[4]
            x6 = actual_joint_states[5]

           
            # j1 = round(((actual_joint_states[0]) - (controller.axis_r.x * multiplier)), 3)
            j1 = actual_joint_states[0] - (controller.axis_r.x * multiplier)
            # j2 = round(((actual_joint_states[1]) - (controller.axis_r.y * multiplier)), 3)
            j2 = actual_joint_states[1] - (controller.axis_r.y * multiplier)
         
            # j3 = round(((actual_joint_states[2]) - (controller.axis_l.y * multiplier)), 3)
            j3 = actual_joint_states[2] - (controller.axis_l.y * multiplier)
            # j4 = round(((actual_joint_states[3]) + (controller.axis_l.x * multiplier)), 3)
            j4 = actual_joint_states[3] + (controller.axis_l.x * multiplier)

            if controller.trigger_l._value:         
                # j5 = round(((actual_joint_states[4]) + (controller.trigger_l._value * multiplier)), 3)
                j5 = actual_joint_states[4] + (controller.trigger_l._value * multiplier)
            if controller.trigger_r._value:
                # j5 = round(((actual_joint_states[4]) - (controller.trigger_r._value * multiplier)), 3)
                j5 = actual_joint_states[4] - (controller.trigger_r._value * multiplier)

            if controller.button_trigger_l._value:
                # j6 = round(((actual_joint_states[5]) - (controller.button_trigger_l._value * multiplier)), 3)
                j6 = actual_joint_states[5] - (controller.button_trigger_l._value * multiplier)
            if controller.button_trigger_r._value:
                # j6 = round(((actual_joint_states[5]) + (controller.button_trigger_r._value * multiplier)), 3)
                j6 = actual_joint_states[5] + (controller.button_trigger_r._value * multiplier)

            # If new value "much" bigger than old one /new one--> publish/, if not /actual --> publish/
            if (big_enough(j1, actual_joint_states[0])):
                x1 = j1
            else:
                x1 = actual_joint_states[0]

            if (big_enough(j2, actual_joint_states[1])):
                x2 = j2
            else:
                x2 = actual_joint_states[1]

            if (big_enough(j3, actual_joint_states[2])):
                x3 = j3
            else:
                x3 = actual_joint_states[2]

            if (big_enough(j4, actual_joint_states[3])):
                x4 = j4
            else:
                x4 = actual_joint_states[3]

            if (big_enough(j5, actual_joint_states[4])):
                x5 = j5
            else:
                x5 = actual_joint_states[4]

            if (big_enough(j6, actual_joint_states[5])):
                x6 = j6
            else:
                x6 = actual_joint_states[5]


            mqttpublisher.publish_move_joints(MQTT_reciever.client, 0.5, x1, x2, x3, x4, x5, x6)
            # print('Joints Published: ', x1, x2, x3, x4, x5, x6)

        time.sleep(0.035)    


    # signal.pause()
    pass

