# ROS_MQTT_JOY


itt elérhető a ROS/Gazebo a robothoz
https://docs.niryo.com/dev/ros/source/simulation.html

a szimuláció futtatásához a parancs:
roslaunch niryo_robot_bringup desktop_gazebo_simulation.launch

Külön terminálban fut a joy_control_r (a ROS Node az MQTT kommunikációhoz)

és egy harmadikban az MQTT az xbox360 kontrollerhez. A joy controller client mappából:
python3 joy_Controller.py

MQTT Broker IP mindket oldalon a config.py-ban állítható

ezutóbbihoz szükséges egy python package
pip3 install -U xbox360controller
  (innen: https://pypi.org/project/xbox360controller/)
